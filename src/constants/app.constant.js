export const appConstants = {
  BASE_URL: "http://arminkhodaei.com/api",
  FILES_URL: "http://arminkhodaei.com",

  _BASE_URL: "http://arminkhodaei.com/api/v1/manage",
  _FILES_URL: "http://arminkhodaei.com",

  APP_NAME: "انتخاب کالا",
  APP_URL: "http://entekhabkala.com",
  APP_VERSION: "1.0",

  // pusher
  PUSHER_APP_KEY: "d5129de153be80801973",
  PUSHER_APP_CLUSTER: "ap1",

  // auth
  USER_COOKIE_KEY: "__le_d0",
  TOKEN_COOKIE_KEY: "_ew_kjz5",

  // colors
  MAIN_COLOR_NAME: "main",
  MAIN_COLOR_CODE: "#06d96f",
  SECONDARY_COLOR_NAME: "secondary",
  SECONDARY_COLOR_CODE: "#05d3bb",
  DARK_COLOR_NAME: "dark",
  DARK_COLOR_CODE: "#30322f",
  LIGHT_COLOR_NAME: "light",
  LIGHT_COLOR_CODE: "#fff"
};
