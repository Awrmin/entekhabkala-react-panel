import Dashboard from "views/Dashboard/Dashboard.jsx";
import Notifications from "views/Notifications/Notifications.jsx";
import Icons from "views/Icons/Icons.jsx";
import Typography from "views/Typography/Typography.jsx";
import TableList from "views/TableList/TableList.jsx";
import Maps from "views/Maps/Maps.jsx";
import UserPage from "views/UserPage/UserPage.jsx";
import Products from "../views/Products/Products";
import AddProducts from "../views/Products/AddProduct";
import Categories from "../views/Categories/Category";
import Guarantees from "../views/Guarantees/Guarantee";

var dashRoutes = [
  {
    path: "/dashboard",
    name: "داشبورد",
    icon: "nc-icon nc-bank",
    component: Dashboard
  },
  // {
  //   path: "/icons",
  //   name: "Icons",
  //   icon: "nc-icon nc-diamond",
  //   component: Icons
  // },
  // { path: "/maps", name: "Maps", icon: "nc-icon nc-pin-3", component: Maps },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   icon: "nc-icon nc-bell-55",
  //   component: Notifications
  // },
  // {
  //   path: "/user-page",
  //   name: "User Profile",
  //   icon: "nc-icon nc-single-02",
  //   component: UserPage
  // },
  // {
  //   path: "/tables",
  //   name: "Table List",
  //   icon: "nc-icon nc-tile-56",
  //   component: TableList
  // },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   icon: "nc-icon nc-caps-small",
  //   component: Typography
  // },
  {
    path: '/products',
    name: 'محصولات',
    icon: 'nc-icon nc-app',
    component: Products
  },
  {
    path: '/add-product',
    name: 'افزودن محصول',
    icon: 'nc-icon nc-simple-add',
    component: AddProducts
  },
  {
    path: '/categories',
    name: 'دسته بندی ها',
    icon: 'nc-icon nc-bullet-list-67',
    component: Categories
  },
  {
    path: '/guarantees',
    name: 'گارانتی ها',
    icon: 'nc-icon nc-ambulance',
    component: Guarantees
  },
  { redirect: true, path: "/", pathTo: "/dashboard", name: "Dashboard" }
];
export default dashRoutes;
