import React, { Component } from 'react';
import { mediaServices } from "../../services/media.service";
import Dropzone from 'react-dropzone'

class UploadWrapper extends Component {


  constructor(props) {
    super(props)
    this.state = {
      files: []
    }
  }

  componentWillUnmount() {
    // Make sure to revoke the data uris to avoid memory leaks
    this.state.files.forEach(file => URL.revokeObjectURL(file.preview))
  }

  onDrop(files) {
    this.setState({
      files: files.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      }))
    });

    setTimeout(() => this.handleFileUpload(), 100)
  }

  onCancel() {
    this.setState({
      files: []
    });
  }

  handleFileUpload() {

    const files = this.state.files;

    for (let i = 0; i < files.length; i++) {
      const file = files[i];

      file.uploading = true

      this.setState({
        files
      })

      let f_data = new FormData();
      f_data.append('file', file);
      f_data.append('size', '10mb');
      f_data.append('isAvatar', this.props.isAvatar ? 1 : 0);

      mediaServices.upload(f_data)
        .then(response => {
          if (response.data.success) {
            this.props.onUploadSuccess(response.data.result);
            this.setState({
              files: this.state.files.map(item => {
                item.uploaded = true
                return item
              })
            })

          } else {
            this.props.onUploadFail(response.data.error);

            this.setState({
              files: this.state.files.map(item => {
                item.failed = true
                item.error = response.data.error
                return item
              })
            })
          }

        })
        .catch(error => {

          this.setState({
            files: this.state.files.map(item => {
              item.failed = true
              item.error = error
              return item
            })
          })
          console.log(error);
          this.props.onUploadFail(error);
        });

    }

  }

  onThumbnailClick = (file) => {
    let newFiles = this.state.files.filter(item => {
      return file.name !== item.name
    })

    this.setState({
      files: newFiles
    })
  }

  render() {
    const { files } = this.state;

    const thumbs = files.map(file => {

      const isImage = file.type.split('/')[0] === 'image'
      const type = file.type.split('/')[1] === 'x-zip-compressed' ? 'zip' :
        file.type.split('/')[1] === 'plain' ? 'txt' : file.type.split('/')[1] || ''

      return (
        <div
          className={"thumb" + (file.uploaded ? ' uploaded' : '') + (file.failed ? ' failed' : '')}
          key={file.name}
          onClick={() => this.onThumbnailClick(file)}>
          <div className="thumb-inner">
            {isImage ?
              <img src={file.preview} className="thumb-img" alt='file-priview' /> :
              <div className="type-text">{type}</div>}
          </div>
          {file.uploading && !file.uploaded && !file.failed &&
            <span className="uploading-icon">
              <i class="fas fa-spinner fa-lg text-white"></i>
            </span>}
        </div>
      )
    });
    return (
      <div className="UploadWrapper">
        <Dropzone multiple={this.props.multiple}
          accept={this.props.accept || "audio/*,image/*,video/*,.pdf,.zip,.tar,.tar.gz,.txt"}
          onDrop={this.onDrop.bind(this)}>
          {({ getRootProps, getInputProps, isDragActive }) => (
            <div {...getRootProps()} className={"upload-zone" + (isDragActive ? ' active' : '')}>
              <input {...getInputProps()} />
              <p>فایل را بکشید و اینجا رها کنید</p>
            </div>
          )}
        </Dropzone>
        <aside className="thumb-container">
          {thumbs}
        </aside>
      </div>
    )
  }
}

export default UploadWrapper
