import React, { Component } from 'react';

// Dependency imports
import { Table, Card, CardBody, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';

import Button from '../../components/CustomButton/CustomButton';
import axios from 'axios';
import { toast } from 'react-toastify';
import { appConstants } from '../../constants/app.constant';


class Guarantees extends Component {
  constructor(props) {
    super(props);
    this.state = {
      guarantees: [],
      modal: false,
      guaranteeName: ''

    }
  }
  componentDidMount() {
    this.handleGetGuarantees()
  }

  fakeCategory = [
    {
      id: 1,
      picture: 'https://via.placeholder.com/64',
      name: 'محصولات برقی',
      productCount: 15,
      subCategoryCount: 12
    },
    {
      id: 1,
      picture: 'https://via.placeholder.com/64',
      name: 'محصولات برقی',
      productCount: 15,
      subCategoryCount: 12
    },
    {
      id: 1,
      picture: 'https://via.placeholder.com/64',
      name: 'محصولات برقی',
      productCount: 15,
      subCategoryCount: 12
    },
    {
      id: 1,
      picture: 'https://via.placeholder.com/64',
      name: 'محصولات برقی',
      productCount: 15,
      subCategoryCount: 12
    },
  ]

  handleCategoryRemove = (item) => {

    this.setState({
      modal: !this.state.modal
    });

  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });

  }

  handleGetGuarantees = () => {
    axios({
      method: 'get',
      url: appConstants.BASE_URL + '/guarantees'
    })
      .then(({ data }) => {
        if (data.success) {
          this.setState({
            guarantees: data.result
          })
        } else {
          toast.error('خطا در دریافت اطلاعات')
        }
      }).catch(err => {
        console.log(err);
        toast.error('خطا در دریافت اطلاعات')

      })
  }

  handleAddGuarantee = () => {
    axios({
      method: 'post',
      url: appConstants.BASE_URL + '/guarantees',
      data: {
        name: this.state.guaranteeName
      }
    })
      .then(({ data }) => {
        if (data.success) {
          toast.success('گارانتی اضافه شد')
          this.setState({
            guaranteeName: ''
          })
        } else {
          toast.error('خطا در در دریافت اطلاعات')
        }
      })
      .catch(err => {
        console.log(err);
        toast.error('خطا در در دریافت اطلاعات')
      })
  }

  handleGuaranteeChange = (key, value) => {
    this.setState({
      guaranteeName: value
    })
  }

  render() {
    return (
      <div className="content">
        <div className="row">
          <div className="col-md-8">
            <Card>
              <h4 className="m-2 p-1">گارانتی ها</h4>
              <CardBody>
                <Table hover>
                  <thead>
                    <tr>
                      <th>ردیف</th>
                      <th>نام گارانتی</th>
                      <th>تعداد محصولات موجود</th>
                      <th>عملیات</th>
                    </tr>
                  </thead>
                  <tbody>
                    {(this.state.guarantees || []).map((item, key) => {
                      return (
                        <tr>
                          <th scope='row'>{item.id}</th>
                          <th>{item.name}</th>
                          <th>{item.__meta__ && item.__meta__.product_count}</th>
                          <th>
                            <Button
                              color="danger"
                              fab
                              round
                              icon
                              size='sm'
                              onClick={() => this.handleCategoryRemove(item)}>
                              <i class="fas fa-trash"></i>
                            </Button>
                          </th>
                        </tr>
                      )
                    })}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </div>
          <div className="col-md-4">
            <div className="row">
              <div className="col-md-12">
                <Card>
                  <h5 className="m-0 p-2">افزودن گارانتی</h5>
                  <CardBody>
                    <div className="row">
                      <div className="col-md-12">
                        <Input
                          type="text"
                          placeholder="نام گارانتی"
                          onChange={(event) => this.handleGuaranteeChange('name', event.target.value)}
                        />
                      </div>
                      <div className="col-md-12"></div>
                      <div className="col-md-12">
                        <Button className="btn-block" onClick={this.handleAddGuarantee}>
                          ثبت گارانتی
                        </Button>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </div>
            </div>
          </div>
        </div>

        {/* Delete category modal */}
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader className="justify-content-center">حذف گارانتی</ModalHeader>
          <ModalBody className="text-right">
            از حذف این گارانتی مطمعن هستید؟
          </ModalBody>
          <ModalFooter className="justify-content-start">
            <Button color="success" onClick={this.toggle}>حذف کن</Button>{' '}
            <Button color="danger" onClick={this.toggle}>انصراف</Button>
          </ModalFooter>
        </Modal>
        {/* Delete category modal */}
      </div>
    );
  }
}

export default Guarantees;