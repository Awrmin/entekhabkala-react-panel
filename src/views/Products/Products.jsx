import React, { Component } from 'react';
import { Card, CardBody, Button, CardTitle, CardText, CardImg } from 'reactstrap';
import axios from 'axios';
import { toast } from 'react-toastify';
import { appConstants } from '../../constants/app.constant';
class Products extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: []
    }
  }
  componentDidMount() {
    this.handleGetProducts()
  }

  handleGetProducts = () => {
    axios({
      method: 'get',
      url: appConstants.BASE_URL + '/products'
    })
      .then(({ data }) => {
        if (data.success) {
          this.setState({
            products: data.result
          })
        } else {
          toast.error('خطا در دریافت اطلاعات')
        }
      })
      .catch(err => {
        console.log(err);
        toast.error('خطا در دریافت اطلاعات')
      })
  }

  render() {
    const { products } = this.state
    const productItem = (products || []).map((item, key) => (
      <div className="col-md-3">
        <Card>
          <CardImg top width="100%" src={appConstants.FILES_URL + '/' + item.image.source} alt="Card image cap" />
          <CardBody>
            <CardTitle>{item.name}</CardTitle>
            <CardText>{item.model}</CardText>
            <CardText>
              <small className="text-muted">{item.created_at}</small>
            </CardText>
            <div className="text-center mt-5">
              <Button>مشاهده محصول</Button>
            </div>
          </CardBody>
        </Card>
      </div>
    ))

    return (
      <div className="content">
        <div className="row">
          <div className="col-md-12">
            <Card>
              <h5>آخرین محصولات</h5>
              <div className="row px-3">
                {productItem}
              </div>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default Products;