import React, { Component } from 'react';
import { Card, CardBody, Button, } from 'reactstrap';
import { FormGroup, Label, Input, CustomInput } from "reactstrap";
import { ListGroup, ListGroupItem } from 'reactstrap';
import { CirclePicker } from 'react-color';
import axios from 'axios';
import UploadWrapper from "../../components/Uploader/UploadWrapper";
import { toast } from 'react-toastify';


// import Button from "components/CustomButton/CustomButton.jsx";

import ReactCrop from 'react-image-crop';
import 'react-image-crop/dist/ReactCrop.css';
import { mediaServices } from '../../services/media.service';
import { productServices } from '../../services/product.service';
import { appConstants } from '../../constants/app.constant';

class AddProducts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productDetails: {
        name: '',
        model: '',
        price: '',
        quantity: 0,
        category_id: null,
        subcategory_id: null,
        gurantee_id: null,
        gurantee_extra_fee: '',
        post_type: '',
        payment_type: '',
        price_type: '',
        description: ''
      },
      productAttributes: [],
      productColors: [],
      attributeKey: '',
      attributeValue: '',
      debounceTimeout: null,
      debounceDelay: 300,
      background: '#fff',
      mediaData: [],
      categories: [],
      subCategories: [],
      guarantees: []
    }
  }

  postTypes = [
    {
      id: 0,
      key: 'post',
      value: 'post',
      text: 'ارسال پستی'
    },
    {
      id: 1,
      key: 'express',
      value: 'express',
      text: 'ارسال پیشتاز'
    },
    {
      id: 2,
      key: 'bike',
      value: 'bike',
      text: 'ارسال با پیک موتوری'
    },

  ]
  paymentTypes = [
    {
      id: 0,
      key: 'online',
      value: 'online',
      text: 'پرداخت آنلاین'
    },
    {
      id: 1,
      key: 'cash',
      value: 'cash',
      text: 'پرداخت نقدی'
    }
  ]
  priceTypes = [
    {
      id: 0,
      key: 'static',
      value: 'static',
      text: 'قیمت ثابت'
    },
    {
      id: 1,
      key: 'discounted',
      value: 'discounted',
      text: 'قیمت با تخفیف'
    },
    {
      id: 2,
      key: 'call_us',
      value: 'call_us',
      text: 'تماس با ما'
    }
  ]

  handleProductDetailChange = (key, value) => {
    let { productDetails } = this.state

    if(key === 'category_id') {
      this.handleGetSubCategories(value)
    }

    productDetails[key] = value

    this.setState({ productDetails })
  }

  handleAttributeSubmit = () => {
    let { productAttributes, attributeKey, attributeValue } = this.state

    productAttributes.push({ key: attributeKey, value: attributeValue })

    this.setState({ productAttributes })
  }

  handleAttributeChange = (key, value) => {

    let { attributeKey, attributeValue } = this.state

    if (key === 'key') {
      this._debounce(() => {
        this.setState({ attributeKey: value })
      })
    }
    this._debounce(() => {
      this.setState({
        attributeValue: value
      })
    })

  }

  removeAtribute(event, key) {
    event.persist();

    this.setState({
      productAttributes: this.state.productAttributes.filter(attr => {
        return attr.key !== key
      })
    })
  }

  handleChangeComplete = (color, event) => {
    this.setState({ background: color.hex });
  }

  handleAddColor = () => {
    let { productColors, background } = this.state

    this.setState(prevState => ({
      productColors: [...prevState.productColors, background]
    }))

  }

  handleNewFileUploadSuccess = (result = null) => {
    let { mediaData } = this.state

    if (result.thumbnail) {
      result.thumbnail = result.thumbnail.split(',')

      if (result.thumbnail.length === 1)
        result.thumbnail = result.thumbnail[0]

      else if (result.thumbnail.length === 2) {
        result.thumbnail = result.thumbnail[0]
        result.thumbnail_2 = result.thumbnail[1]

      } else if (result.thumbnail.length === 3) {
        result.thumbnail = result.thumbnail[0]
        result.thumbnail_2 = result.thumbnail[1]
        result.thumbnail_3 = result.thumbnail[2]
      }

    }
    mediaData.unshift(result);

    this.setState({ mediaData })
  }

  handleNewFileUploadFail = (result = null) => {
    console.log('failed!');
    console.log(result);
  }

  handleGetCategories = () => {
    axios({
      method: 'get',
      url: appConstants.BASE_URL + '/categories'
    })
      .then(({ data }) => {
        if (data.success) {
          this.setState({
            categories: data.result
          })
        } else {
          console.log('failed fetching categories');

        }

      })
      .catch(err => {
        console.log(err);

      })
  }

  handleGetSubCategories = (id) => {
    axios({
      method: 'get',
      url: appConstants.BASE_URL + '/sub-categories?category_id='+ id,
    })
      .then(({ data }) => {
        if (data.success) {
          this.setState({
            subCategories: data.result
          })
        } else {
          console.log('failed fetching sub categories');

        }

      })
      .catch(err => {
        console.log(err);

      })

  }

  handleGetGuarantees = () => {
    axios({
      method: 'get',
      url: appConstants.BASE_URL + '/guarantees'
    })
      .then(({ data }) => {
        if (data.success) {
          this.setState({
            guarantees: data.result
          })
        } else {
          console.log('failed fetching guarantees');

        }

      })
      .catch(err => {
        console.log(err);

      })

  }

  handleAddProduct = () => {
    let { productDetails } = this.state
    productServices.addProduct({
      name: productDetails.name,
      model: productDetails.model,
      price: productDetails.price,
      quantity: productDetails.quantity,
      discounted_price: '0',
      description: productDetails.description || 'تست',
      is_available: 1,
      post_type: productDetails.post_type,
      payment_type: productDetails.payment_type,
      price_type: productDetails.price_type,
      category_id: productDetails.category_id,
      subcategory_id: productDetails.subcategory_id,
      guarantee_id: productDetails.gurantee_id,
      guarantee_extra_fee: 15000,
      colors: this.state.productColors,
      media_id: this.state.mediaData[0].id,
      images: this.state.mediaData,
      attributes: this.state.productAttributes
    })
    .then(({data}) => {
      console.log(data);
      toast.success('محصول با موفقیت ثبت شد', {autoClose: false})
      
      
    })
    .catch(err => {
      console.log(err);
      toast.error('خطا در ثبت محصول')
      
    })
  }

  componentDidMount() {
    this.handleGetCategories()
    this.handleGetSubCategories()
    this.handleGetGuarantees()
  }



  /**
   *  Function to handle debounce
   * @param func - everything that should run with debounce
   * @private
   */
  _debounce = (func) => {

    let { debounceTimeout, debounceDelay } = this.state;

    clearTimeout(debounceTimeout);
    debounceTimeout = setTimeout(func, debounceDelay, this);

    this.setState({
      debounceTimeout,
    });

  }


  render() {

    let { productAttributes } = this.state


    const atributesItem = productAttributes.map((item, key) => (
      <ListGroupItem
        className="d-flex justify-content-between"
        key={key}>
        <div>
          <span>{item.key}</span>{' - '}
          <span>{item.value}</span>
        </div>
        <Button close onClick={(e) => this.removeAtribute(e, item.key)} />
      </ListGroupItem>
    ))

    return (
      <div className="content">
        <div className="row">
          <div className="col-md-8">
            <Card>
              <h3 className="m-3">اطلاعات اولیه محصول</h3>
              <CardBody>
                <form>
                  <div className="form-row">
                    <FormGroup className="col-md-4">
                      <Label>نام محصول</Label>
                      <Input type="text" placeholder="تلویزیون سامسونگ" onChange={(event) => this.handleProductDetailChange('name', event.target.value)} />
                    </FormGroup>
                    <FormGroup className="col-md-4">
                      <Label>مدل محصول</Label>
                      <Input type="text" placeholder="SMTV-2591" onChange={(event) => this.handleProductDetailChange('model', event.target.value)} />
                    </FormGroup>
                    <FormGroup className="col-md-4">
                      <Label>قیمت اصلی محصول</Label>
                      <Input type="text" placeholder="1500000" onChange={(event) => this.handleProductDetailChange('price', event.target.value)} />
                    </FormGroup>
                  </div>
                  <div className="form-row">
                    <FormGroup className="col-md-4">
                      <Label>دسته بندی محصول</Label>
                      <CustomInput type="select" onChange={(event) => this.handleProductDetailChange('category_id', event.target.value)}>
                        <option value="null">دسته بندی محصول را انتخاب کنید</option>
                        {this.state.categories.map((item, key) => (
                          <option value={item.id} key={key}>{item.name}</option>
                        ))}
                      </CustomInput>
                    </FormGroup>
                    <FormGroup className="col-md-4">
                      <Label>زیر دسته بندی محصول</Label>
                      <CustomInput type="select" onChange={(event) => this.handleProductDetailChange('subcategory_id', event.target.value)}>
                        {this.state.subCategories.map((item, key) => (
                          <option value={item.id} key={key}>{item.name}</option>
                        ))}

                      </CustomInput>
                    </FormGroup>
                    <FormGroup className="col-md-4">
                      <Label>نوع پرداخت</Label>
                      <CustomInput type="select" onChange={(event) => this.handleProductDetailChange('payment_type', event.target.value)}>
                        <option value="null">انتخاب نوع پرداخت</option>
                        {this.paymentTypes.map((item, key) => (
                          <option value={item.value} key={key}>{item.text}</option>
                        ))}
                      </CustomInput>
                    </FormGroup>
                  </div>
                  <div className="form-row">
                    <FormGroup className="col-md-4">
                      <Label>نحوه ارسال</Label>
                      <CustomInput type="select" onChange={(event) => this.handleProductDetailChange('post_type', event.target.value)}>
                        <option value='null'>نحوه ارسال را انتخاب کنید</option>
                        {this.postTypes.map((item, key) => (
                          <option value={item.value} key={key}>{item.text}</option>
                        ))}
                      </CustomInput>
                    </FormGroup>
                    <FormGroup className="col-md-2">
                      <Label>نوع قیمت</Label>
                      <CustomInput type="select" onChange={(event) => this.handleProductDetailChange('price_type', event.target.value)}>
                        <option value="null">نوع قیمت را انتخاب کنید</option>
                        {this.priceTypes.map((item, key) => (
                          <option value={item.value} key={key}>{item.text}</option>
                        ))}
                      </CustomInput>
                    </FormGroup>
                    <FormGroup className="col-md-4">
                      <Label>گارانتی محصول</Label>
                      <CustomInput type="select" onChange={(event) => this.handleProductDetailChange('gurantee_id', event.target.value)}>
                        <option value="null">گارانتی محصول را انتخاب کنید</option>
                        {this.state.guarantees.map((item, key) => (
                          <option value={item.id} key={key}>{item.name}</option>
                        ))}
                      </CustomInput>
                    </FormGroup>
                    <FormGroup className="col-md-2">
                      <Label for="inputZip">هزینه گارانتی</Label>
                      <Input type="text" id="inputZip" />
                    </FormGroup>
                  </div>
                </form>
              </CardBody>
            </Card>
            <div className="col-md-12 px-0">
              <Card>
                <h5 className="m-0 p-2">عکس های محصول</h5>
                <CardBody>
                  <div className="row">
                    <div className="col-md-12">
                      <UploadWrapper size={'mini'}
                        onUploadSuccess={this.handleNewFileUploadSuccess}
                        onUploadFail={this.handleNewFileUploadFail}
                      />
                    </div>
                  </div>
                </CardBody>
              </Card>
            </div>
            <div className="col-md-12 px-0">
              <Card>
                <CardBody>
                  <Button className="btn-block btn-info" onClick={this.handleAddProduct}>ثبت محصول</Button>
                </CardBody>
              </Card>
            </div>
          </div>
          <div className="col-md-4">
            <Card>
              <h5 className="m-0 p-2">ویژگی های محصول</h5>
              <CardBody>
                <div className="form-row">
                  <FormGroup className="col-md-6">
                    <Input
                      type="text"
                      placeholder="نام ویژگی"
                      onChange={(event) => this.handleAttributeChange('key', event.target.value)}
                    />
                  </FormGroup>
                  <FormGroup className="col-md-6">
                    <Input
                      type="text"
                      placeholder="مقدار ویژگی"
                      onChange={(event) => this.handleAttributeChange('value', event.target.value)}
                    />
                  </FormGroup>
                  <FormGroup className="col-md-12">
                    <Button className="m-0 btn-block" type='button' onClick={() => this.handleAttributeSubmit()}>افزودن ویژگی</Button>
                  </FormGroup>
                </div>
                <ListGroup className="p-0">
                  {atributesItem}
                </ListGroup>
              </CardBody>
            </Card>
            <Card>
              <h5 className="m-0 px-2 py-1">رنگ های محصول</h5>
              <CardBody>
                <div className="row">
                  <div className="col-md-12">
                    <CirclePicker
                      className="mx-auto mb-3"
                      color={this.state.background}
                      onChangeComplete={this.handleChangeComplete}
                    />
                    <div className="row">
                      <div className="col-md-12">
                        <label>هزینه اضافی برای این رنگ</label>
                        <Input className="mb-2" placeholder="15000"></Input>
                      </div>
                    </div>
                    <Button
                      className="m-0 mt-3 mb-2 btn-block"
                      type='button' onClick={this.handleAddColor}>افزودن رنگ</Button>
                  </div>
                </div>
              </CardBody>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default AddProducts;