import React, { Component } from 'react';

// Dependency imports
import { Table, Card, CardBody, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, CustomInput } from 'reactstrap';
import Button from '../../components/CustomButton/CustomButton';
import axios from 'axios'
import { toast } from 'react-toastify';
import { appConstants } from '../../constants/app.constant';

class Categories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category_id: null,
      subcategory_name: '',
      category_name: '',
      modal: false

    }
  }

  componentDidMount() {
    this.handleGetCategories()
  }


  handleCategoryRemove = (item) => {

    this.setState({
      modal: !this.state.modal
    });


    console.log(item);

  }

  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  handleGetCategories = () => {
    axios({
      method: 'get',
      url: appConstants.BASE_URL + '/categories',
      params: {
        with: 'image',
        withCount: 'product,subCategory'
      }
    })
      .then(({ data }) => {
        if (data.success) {
          this.setState({
            categories: data.result
          })
        } else {
          console.log('failed fetching categories');
          toast.error('خطا در دریافت اطلاعات')

        }

      })
      .catch(err => {
        console.log(err);
        toast.error('خطا در دریافت اطلاعات')

      })
  }

  handleCategoryChange = (key, value) => {

    if (key === 'category_name') {
      this.setState({
        category_name: value
      })
    }

    if (key === 'category_id') {
      this.setState({
        category_id: value
      })
    }

    if (key === 'subcategory_name') {
      this.setState({
        subcategory_name: value
      })
    }
  }

  handleCategorySubmit = () => {

    axios({
      method: 'post',
      url: appConstants.BASE_URL + '/categories',
      data: {
        name: this.state.category_name
      }
    })
      .then(({ data }) => {
        if (data.success) {
          toast.success('دسته بندی با موفقیت اضافه شد')
          this.setState({
            category_name: ''
          })
        }
      })
      .catch(err => {
        console.log(err);
        toast.error('خطا در ثبت دسته بندی')

      })
  }

  handleSubCategorySubmit = () => {
    axios({
      method: 'post',
      url: appConstants.BASE_URL + '/sub-categories',
      data: {
        name: this.state.subcategory_name,
        category_id: this.state.category_id
      }
    })
      .then(({ data }) => {
        if (data.success) {
          toast.success('زیر دسته بندی با موفقیت اضافه شد')
          this.setState({
            subcategory_name: ''
          })
        }
      })
      .catch(err => {
        console.log(err);
        toast.error('خطا در ثبت زیر دسته بندی')

      })
  }




  render() {
    return (
      <div className="content">
        <div className="row">
          <div className="col-md-8">
            <Card>
              <h4 className="m-2 p-1">دسته بندی ها</h4>
              <CardBody>
                <Table hover>
                  <thead>
                    <tr>
                      <th>ردیف</th>
                      <th>عکس دسته بندی</th>
                      <th>نام دسته بندی</th>
                      <th>تعداد محصولات موجود</th>
                      <th>تعداد زیر دسته بندی ها</th>
                      <th>عملیات</th>
                    </tr>
                  </thead>
                  <tbody>
                    {((this.state.categories) || []).map((item, key) => {
                      return (
                        <tr>
                          <th scope='row'>{item.id}</th>
                          {item.image && <th><img src={item.image && appConstants.FILES_URL + '/' + item.image.source} alt="no-image" width='60px' height='60px' /></th>}
                          {!item.image && <th><img src="https://2ndlifellc.com/wp-content/uploads/2017/07/no-image.jpg" alt="no-image" width='60px' height='60px' /></th>}
                          <th>{item.name}</th>
                          <th>{item.__meta__ ? item.__meta__.product_count : 0}</th>
                          <th>{item.__meta__ ? item.__meta__.subCategory_count : 0}</th>
                          <th>
                            <Button
                              color="danger"
                              fab
                              round
                              icon
                              size='sm'
                              onClick={() => this.handleCategoryRemove(item)}>
                              <i class="fas fa-trash"></i>
                            </Button>
                          </th>
                        </tr>
                      )
                    })}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </div>
          <div className="col-md-4">
            <div className="row">
              <div className="col-md-12">
                <Card>
                  <h5 className="m-0 p-2">افزودن دسته بندی</h5>
                  <CardBody>
                    <div className="row">
                      <div className="col-md-12">
                        <Input
                          type="text"
                          placeholder="نام دسته بندی"
                          onChange={(event) => this.handleCategoryChange('category_name', event.target.value)}
                        />
                      </div>
                      <div className="col-md-12"></div>
                      <div className="col-md-12">
                        <Button className="btn-block" onClick={this.handleCategorySubmit}>
                          ثبت دسته بندی
                        </Button>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </div>
              <div className="col-md-12">
                <Card>
                  <h5 className="m-0 p-2">افزودن زیر دسته بندی</h5>
                  <CardBody>
                    <div className="row">
                      <div className="col-md-12">
                        <Input
                          className="mb-3"
                          type="text"
                          placeholder="نام زیر دسته بندی"
                          onChange={(event) => this.handleCategoryChange('subcategory_name', event.target.value)}
                        />
                        <CustomInput type="select" onChange={(event) => this.handleCategoryChange('category_id', event.target.value)}>
                          <option value="null">دسته بندی محصول را انتخاب کنید</option>
                          {(this.state.categories || []).map((item, key) => (
                            <option value={item.id} key={key}>{item.name}</option>
                          ))}
                        </CustomInput>

                      </div>
                      <div className="col-md-12"></div>
                      <div className="col-md-12">
                        <Button className="btn-block" onClick={this.handleSubCategorySubmit}>
                          ثبت زیر دسته بندی
                        </Button>
                      </div>
                    </div>
                  </CardBody>
                </Card>
              </div>
            </div>
          </div>
        </div>

        {/* Delete category modal */}
        <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
          <ModalHeader className="justify-content-center">حذف دسته بندی</ModalHeader>
          <ModalBody className="text-right">
            از حذف این دسته بندی مطمعن هستید؟
          </ModalBody>
          <ModalFooter className="justify-content-start">
            <Button color="success" onClick={this.toggle}>حذف کن</Button>{' '}
            <Button color="danger" onClick={this.toggle}>انصراف</Button>
          </ModalFooter>
        </Modal>
        {/* Delete category modal */}
      </div>
    );
  }
}

export default Categories;