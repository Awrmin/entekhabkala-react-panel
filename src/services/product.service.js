import * as axios from "axios";
import { appConstants } from "../constants/app.constant";

const addProduct = data => {
  return axios({
    method: 'post',
    url: appConstants.BASE_URL + `/products`,
    data
  });
}

export const productServices = {
  addProduct,
};
