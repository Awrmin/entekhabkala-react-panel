import * as axios from "axios";
import { appConstants } from "../constants/app.constant";

const upload = data => {
  return axios({
    method: 'post',
    url: appConstants.BASE_URL + `/upload`,
    headers: { 'Content-Type': 'multipart/form-data' },
      data
  });
}
const getAllMedia = (params = {}) => {
  return axios({
      method: 'get',
      url: `http://arminkhodaei.com/api/media`,
      params
  })
}
export const mediaServices = {
  upload,
  getAllMedia
};
